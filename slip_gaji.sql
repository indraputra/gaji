-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Oct 18, 2018 at 07:40 AM
-- Server version: 10.1.13-MariaDB
-- PHP Version: 5.6.23

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `slip_gaji`
--

-- --------------------------------------------------------

--
-- Table structure for table `master_gaji_karyawan`
--

CREATE TABLE `master_gaji_karyawan` (
  `id_karyawan` int(11) NOT NULL,
  `nip` varchar(191) NOT NULL,
  `nama_karyawan` varchar(191) NOT NULL,
  `jabatan` varchar(191) NOT NULL,
  `bagian` varchar(191) NOT NULL,
  `fungsional` varchar(191) NOT NULL,
  `Gaji_Pokok` int(11) DEFAULT NULL,
  `Tunjangan_Jabatan` int(11) DEFAULT NULL,
  `Tunjangan_Transport` int(11) DEFAULT NULL,
  `Tunjangan_Kehadiran` int(11) DEFAULT NULL,
  `Tunjangan_Kesehatan` int(11) DEFAULT NULL,
  `Tunjangan_Komunikasi` int(11) DEFAULT NULL,
  `DL_Dalam_Kota` int(11) DEFAULT NULL,
  `DL_Luar_Kota` int(11) DEFAULT NULL,
  `Lembur_Hari_Kerja` int(11) DEFAULT NULL,
  `Lembur_Hari_Libur` int(11) DEFAULT NULL,
  `create_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `master_gaji_karyawan`
--

INSERT INTO `master_gaji_karyawan` (`id_karyawan`, `nip`, `nama_karyawan`, `jabatan`, `bagian`, `fungsional`, `Gaji_Pokok`, `Tunjangan_Jabatan`, `Tunjangan_Transport`, `Tunjangan_Kehadiran`, `Tunjangan_Kesehatan`, `Tunjangan_Komunikasi`, `DL_Dalam_Kota`, `DL_Luar_Kota`, `Lembur_Hari_Kerja`, `Lembur_Hari_Libur`, `create_at`) VALUES
(2, '09.10', 'saboys', 'tentara', 'prajurit', 'militer', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-10-18 04:22:48'),
(3, '10.07', 'tesla', 'junior', 'tester', 'software', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-10-18 05:31:46');

-- --------------------------------------------------------

--
-- Table structure for table `master_karyawan`
--

CREATE TABLE `master_karyawan` (
  `id_karyawan` int(11) NOT NULL,
  `NIP` int(11) NOT NULL,
  `Nama` varchar(60) NOT NULL,
  `Jabatan` varchar(60) NOT NULL,
  `Bagian` varchar(60) NOT NULL,
  `Fungsional` varchar(60) NOT NULL,
  `create_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `master_karyawans`
--

CREATE TABLE `master_karyawans` (
  `id_karyawan` int(10) UNSIGNED NOT NULL,
  `nip` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nama_karyawan` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `jabatan` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `bagian` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `fungsional` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `master_karyawans`
--

INSERT INTO `master_karyawans` (`id_karyawan`, `nip`, `nama_karyawan`, `jabatan`, `bagian`, `fungsional`, `created_at`, `updated_at`, `deleted_at`) VALUES
(2, '09.10', 'saboys', 'tentara', 'prajurit', 'militer', '2018-10-17 21:22:48', '2018-10-17 22:22:52', '2018-10-17 22:22:52'),
(3, '10.07', 'tesla', 'junior', 'tester', 'software', '2018-10-17 22:31:46', '2018-10-17 22:31:46', NULL);

--
-- Triggers `master_karyawans`
--
DELIMITER $$
CREATE TRIGGER `delete_pegawai` AFTER DELETE ON `master_karyawans` FOR EACH ROW DELETE FROM master_gaji_karyawan WHERE id_karyawan = OLD.id_karyawan
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `edit_pegawai` AFTER UPDATE ON `master_karyawans` FOR EACH ROW UPDATE master_gaji_karyawan SET 
nip = NEW.nip,
nama_karyawan = NEW.nama_karyawan, 
jabatan = NEW.jabatan,
bagian = NEW.Bagian,
fungsional = NEW.fungsional
WHERE master_gaji_karyawan.id_karyawan = OLD.id_karyawan
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `tambah_Pegawai` AFTER INSERT ON `master_karyawans` FOR EACH ROW INSERT INTO
master_gaji_karyawan
SET
id_karyawan = NEW.id_karyawan,
nip = NEW.nip,
nama_karyawan = NEW.nama_karyawan,
bagian = NEW.bagian,
fungsional = NEW.fungsional,
jabatan = NEW.jabatan
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(7, '2014_10_12_000000_create_users_table', 1),
(8, '2014_10_12_100000_create_password_resets_table', 1),
(9, '2018_10_16_072515_create_master_karyawans_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'admin@gmail.com', '$2y$10$78OjQAH1toK.lFDI2amwtOmXR0.50rvmzai27.ls6GRP4SOYj4U3y', NULL, '2018-10-17 21:20:39', '2018-10-17 21:20:39');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `master_gaji_karyawan`
--
ALTER TABLE `master_gaji_karyawan`
  ADD PRIMARY KEY (`id_karyawan`),
  ADD KEY `Nama` (`nama_karyawan`),
  ADD KEY `Jabatan` (`jabatan`),
  ADD KEY `Bagian` (`bagian`),
  ADD KEY `Fungsional` (`fungsional`),
  ADD KEY `NIP` (`nip`);

--
-- Indexes for table `master_karyawan`
--
ALTER TABLE `master_karyawan`
  ADD PRIMARY KEY (`id_karyawan`),
  ADD KEY `Nama` (`Nama`),
  ADD KEY `Jabatan` (`Jabatan`),
  ADD KEY `Fungsional` (`Fungsional`),
  ADD KEY `Bagian` (`Bagian`);

--
-- Indexes for table `master_karyawans`
--
ALTER TABLE `master_karyawans`
  ADD PRIMARY KEY (`id_karyawan`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `master_karyawan`
--
ALTER TABLE `master_karyawan`
  MODIFY `id_karyawan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `master_karyawans`
--
ALTER TABLE `master_karyawans`
  MODIFY `id_karyawan` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
