<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MasterGaji extends Model
{
    use SoftDeletes;
    protected $dates=['delete_at'];
    protected $table='master_gaji_karyawan';
    protected $primaryKey='id_gaji';
    protected $fillable=[
        'bagian',
        'tunjangan_jabatan',
        'tunjangan_transport',
        'tunjangan_kehadiran',
        'tunjangan_kesehatan',
        'tunjangan_komunikasi',
        'gaji_pokok',
        'dl_dalam_kota',
        'dl_luar_kota',
        'lembur_hari_libur',
        'lembur_hari_kerja'
    ];
}
