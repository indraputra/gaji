<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GajiPerBulan extends Model
{
    protected $table = 'gaji_per_bulan';
    protected $primaryKey = 'id_gpb';
    protected $fillable = [
        'id',
        'total_tunjangan',
        'total_dl',
        'total_lembur',
    	'total_gaji',
    	'hari_masuk',
    	'total_jam_kerja',
    	'absen',
    	'izin',
    	'terlambat',
    	'cuti',
    	'dl_dalam_kota',
    	'dl_luar_kota',
    	'lembur_hari_kerja',
    	'lembur_hari_libur'
    ];
}
