<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GajiKaryawan extends Model
{
    protected $table='karyawan';
    protected $primaryKey='id';
    protected $fillable=['id_gaji', 'id_karyawan'];
}
