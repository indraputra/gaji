<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MasterKaryawan extends Model
{
    use SoftDeletes;
    protected $dates=['delete_at'];
    protected $table='master_karyawans';
    protected $primaryKey='id_karyawan';
    protected $fillable=['nama_karyawan','nip','jabatan','fungsional'];
}
