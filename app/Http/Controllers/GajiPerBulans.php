<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\GajiPerBulan;
use App\GajiKaryawan;
use Excel;
use DB;
use PDF;

class GajiPerBulans extends Controller
{

    public function addgaji($id)
    {
        $gajikaryawan = GajiKaryawan::where('id',$id)->join('master_gaji_karyawan','karyawan.id_gaji','=','master_gaji_karyawan.id_gaji')
        ->join('master_karyawans','karyawan.id_karyawan','=','master_karyawans.id_karyawan')
        ->select('master_gaji_karyawan.*','master_karyawans.*','karyawan.*')
        ->get();
        return view('gajiperbulan.addgaji', compact('gajikaryawan'));
    }

    public function store()
    {
    	$t1 = request('t1');
    	$t2 = request('t2');
    	$t3 = request('t3');
    	$t4 = request('t4');
    	$t5 = request('t5');
        $total_tunjangan = $t1+$t2+$t3+$t4+$t5;
    	$gaji_pokok = request('gaji_pokok');

        $dl_dalam_kota = request('dl_dalam_kota');
        $dl_luar_kota = request('dl_luar_kota');
    	$ddk = request('ddk');
    	$dlk = request('dlk');
        $total_ddk = $dl_dalam_kota * $ddk;
        $total_dlk = $dl_luar_kota * $dlk;
        $total_dl = $total_ddk+$total_dlk;

        $lembur_hari_kerja = request('lembur_hari_kerja');
        $lembur_hari_libur = request('lembur_hari_libur');
        $lhk = request('lhk');
        $lhl = request('lhl');
        $total_lembur_kerja = $lembur_hari_kerja*$lhk;
        $total_lembur_libur = $lembur_hari_libur*$lhl;
        $total_lembur = $total_lembur_libur+$total_lembur_kerja;

    	$total_gaji = $total_tunjangan+$total_dl+$total_lembur+$gaji_pokok;
    	GajiPerBulan::create([
    		'id'=>request('id'),
    		'total_gaji'=>$total_gaji,
            'total_dl'=>$total_dl,
            'total_tunjangan'=>$total_tunjangan,
            'total_lembur'=>$total_lembur,
    		'hari_masuk'=>request('hari_masuk'),
    		'total_jam_kerja'=>request('total_jam_kerja'),
    		'absen'=>request('absen'),
    		'izin'=>request('izin'),
    		'terlambat'=>request('terlambat'),
    		'cuti'=>request('cuti'),
    		'dl_dalam_kota'=>request('dl_dalam_kota'),
    		'dl_luar_kota'=>request('dl_luar_kota'),
    		'lembur_hari_kerja'=>request('lembur_hari_kerja'),
    		'lembur_hari_libur'=>request('lembur_hari_libur')
    	]);

        return redirect()->route('gajikaryawan.index');
    }

    public function show($id)
    {
        $gajikaryawan = GajiPerBulan::where('id',$id)->get();
        $gajiperbulan = GajiPerBulan::where('id', $id)->get();
        $absen = GajiPerBulan::where('id', $id)->get();
        $exportpdf = GajiPerBulan::where('id', $id)->get();
        return view('gajiperbulan.show', compact('gajiperbulan','absen','gajikaryawan','exportpdf'));
    }

    public function pdf($id)
    {
        $gajiperbulan = GajiPerBulan::where('id',$id)->get();
        $pdf = PDF::loadView('gajiperbulan.pdf',compact('gajiperbulan'),['GajiPerBulan' => $gajiperbulan]);
        return $pdf->download('gajiperbulan.pdf');
    }

    public function absen($id_gpb)
    {
        $absen = GajiPerBulan::where('id_gpb',$id_gpb)->get();

        return view('gajiperbulan.absen', compact('absen'));
    }

    public function search(Request $request,$id)
    {
      $search = $request->get('search');
      $gajiperbulan = GajiPerBulan::where('id', $id)->get();
      $result = GajiPerBulan::where([['id',$id],['total_gaji','like','%'.$search.'%']])
      ->orWhere([['id',$id],['created_at','like','%'.$search.'%']])
      ->get();
      $absen = GajiPerBulan::where('id', $id)->get();
      $exportpdf = GajiPerBulan::where('id', $id)->get();
      return view('gajiperbulan.result', compact('result','absen','gajiperbulan','exportpdf'));
    }

    public function destroy($id_gpb)
    {
        $gajiperbulan = GajiPerBulan::where('id_gpb',$id_gpb)->first();
        $gajiperbulan->delete();
        return redirect()->route('gajikaryawan.index');
    }

    public function excel(){
        Excel::create('gajiperbulan', function($excel) {

        $excel->sheet('Sheet 1', function($sheet) {

            $gaji_per_bulan=DB::table('gaji_per_bulan')
            ->join("karyawan","gaji_per_bulan.id","=","karyawan.id")
            ->select("karyawan.*","gaji_per_bulan.*")
            ->get();
                foreach($gaji_per_bulan as $gpb) {
                 $data[] = array(
                    $gpb->id_gpb,
                    $gpb->id,
                    $gpb->total_tunjangan,
                    $gpb->total_dl,
                    $gpb->total_lembur,
                    $gpb->total_gaji,
                    $gpb->hari_masuk,
                    $gpb->total_jam_kerja,
                    $gpb->izin,
                    $gpb->terlambat,
                    $gpb->cuti,
                    $gpb->dl_dalam_kota,
                    $gpb->dl_luar_kota,
                    $gpb->lembur_hari_kerja,
                    $gpb->lembur_hari_libur,
                    $gpb->created_at,
                );
            }
            $headings = array('id_gpb', 'id', 'total_tunjangan', 'total_dl', 'total_lembur','total_gaji','hari_masuk','total_jam_kerja','izin','terlambat','cuti','dl_dalam_kota','dl_luar_kota','lembur_hari_kerja','lembur_hari_libur','created_at');
            $sheet->prependRow(1, $headings);
            $sheet->fromArray($data, null, 'A1', false, false)->prependRow(1, $headings);
        });
    })->export('xls');
    }

}
