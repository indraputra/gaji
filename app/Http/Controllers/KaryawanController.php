<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\MasterKaryawan;
use App\Karyawan;
use Excel;
use PDF;

class KaryawanController extends Controller
{
    public function index()
    {
        $karyawan = MasterKaryawan::all();
        return view('karyawan.index', compact('karyawan'));
    }

    public function create()
    {
        $karyawan = MasterKaryawan::all();
        return view('karyawan.create', compact('master_karyawans'));
    }

    public function store()
    {
        MasterKaryawan::create([
            'nip'=>request('nip'),
            'nama_karyawan'=>request('nama_karyawan'),
            'jabatan'=>request('jabatan'),
            'fungsional'=>request('fungsional')
        ]);

        return redirect()->route('karyawan.index');
    }

    public function edit($id_karyawan) 
    {
        $karyawan = MasterKaryawan::where('id_karyawan', $id_karyawan)->get();
        return view('karyawan.edit', compact('karyawan'));
    }

    public function update(Request $request, $id_karyawan) 
    {
        $karyawan = MasterKaryawan::where('id_karyawan', $id_karyawan)->first();
        $karyawan->nama_karyawan = $request->nama_karyawan;
        $karyawan->nip = $request->nip;
        $karyawan->jabatan = $request->jabatan;
        $karyawan->fungsional = $request->fungsional;
        $karyawan->save();
        return redirect()->route('karyawan.index')->with('alert-success', 'Data berhasil diubah!');
    }

    public function destroy($id_karyawan)
    {
        $karyawan = MasterKaryawan::where('id_karyawan',$id_karyawan)->first();
        $karyawan->delete();
        return redirect()->route('karyawan.index');
    }

    public function destroyall(Request $request)
    {
        $delid = $request->input('delid');
        MasterKaryawan::whereIn('id_karyawan',$delid)->delete();
        return redirect()->route('karyawan.index');
    }

    public function search(Request $request)
    {
      $search = $request->get('search');
      $result = MasterKaryawan::where('nama_karyawan','like','%'.$search.'%')
      ->orWhere('nip','like','%'.$search.'%')
      ->orWhere('jabatan','like','%'.$search.'%')
      ->orWhere('fungsional','like','%'.$search.'%')
      ->get();
      return view('karyawan.result', compact('result'));
    }

    public function excel()
    {
        $karyawan =  MasterKaryawan::select('id_karyawan','nama_karyawan','nip','jabatan','fungsional','created_at','updated_at')->get(); 
        return Excel::create('karyawan',  function($excel) use($karyawan){
              $excel->sheet('mysheet',  function($sheet) use($karyawan){
              $sheet->fromArray($karyawan);
              });
        })->download('xls');
    }

    public function pdf()
    {
        $karyawan = MasterKaryawan::all();
        $pdf = PDF::loadView('karyawan.pdf',compact('karyawan'),['Karyawan' => $karyawan]);
        return $pdf->download('karyawan.pdf');
    }


}
