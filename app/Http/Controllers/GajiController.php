<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\MasterGaji;
use App\MasterKaryawan;
use App\Gaji;
use App\Karyawan;
use Excel;
use PDF;

class GajiController extends Controller
{
    public function index()
    {
        $gaji = MasterGaji::all();
        return view('gaji.index', compact('gaji'));
    }

    public function create()
    {
        $gaji = MasterGaji::all();
        return view('gaji.create', compact('gaji'));
    }

    public function store()
    {
        $a = request('tunjangan_jabatan');
        $b = request('tunjangan_transport');
        $c = request('tunjangan_kehadiran');
        $d = request('tunjangan_kesehatan');
        $e = request('tunjangan_komunikasi');
        $gajipokok = request('gaji_pokok');
        MasterGaji::create([
            'bagian'=>request('bagian'),
            'tunjangan_jabatan'=> $a,
            'tunjangan_transport'=> $b,
            'tunjangan_kehadiran'=> $c,
            'tunjangan_kesehatan'=> $d,
            'tunjangan_komunikasi'=> $e,
            'gaji_pokok'=> $gajipokok,
            'dl_dalam_kota'=>request('dl_dalam_kota'),
            'dl_luar_kota'=>request('dl_luar_kota'),
            'lembur_hari_kerja'=>request('lembur_hari_kerja'),
            'lembur_hari_libur'=>request('lembur_hari_libur')
        ]);

        return redirect()->route('gaji.index');
    }

    public function edit($id_gaji)
    {

        $gaji = MasterGaji::where('id_gaji', $id_gaji)->get();
        return view('gaji.edit', compact('gaji'));
    }

    public function update(Request $request, $id_gaji)
    {
        $gaji = MasterGaji::where('id_gaji', $id_gaji)->first();
        $gaji->bagian = $request->bagian;
        $gaji->tunjangan_jabatan = $request->tunjangan_jabatan;
        $gaji->tunjangan_transport = $request->tunjangan_transport;
        $gaji->tunjangan_kehadiran = $request->tunjangan_kehadiran;
        $gaji->tunjangan_kesehatan = $request->tunjangan_kesehatan;
        $gaji->tunjangan_komunikasi = $request->tunjangan_komunikasi;
        $gaji->gaji_pokok = $request->gaji_pokok;
        $gaji->dl_dalam_kota = $request->dl_dalam_kota;
        $gaji->dl_luar_kota = $request->dl_luar_kota;
        $gaji->lembur_hari_kerja = $request->lembur_hari_kerja;
        $gaji->lembur_hari_libur = $request->lembur_hari_libur;
        $gaji->save();
        return redirect()->route('gaji.index')->with('alert-success', 'Data berhasil diubah!');
    }

    public function destroy($id_gaji)
    {
        $gaji = MasterGaji::where('id_gaji',$id_gaji)->first();
        $gaji->delete();
        return redirect()->route('gaji.index');
    }

    public function destroyall(Request $request)
    {
        $delid = $request->input('delid');
        MasterGaji::whereIn('id_gaji', $delid)->delete();
        return redirect()->route('gaji.index');
    }

    public function search(Request $request)
    {
      $search = $request->get('search');
      $result = MasterGaji::where('bagian','like','%'.$search.'%')
      ->orwhere('gaji_pokok','like','%'.$search.'%')
      ->get();
      return view('gaji.result', compact('result'));
    }

    public function excel()
    {
        $gaji =  MasterGaji::select('id_gaji','bagian','tunjangan_jabatan','tunjangan_transport','tunjangan_kehadiran','tunjangan_kesehatan','tunjangan_komunikasi','dl_dalam_kota','dl_luar_kota','lembur_hari_kerja','lembur_hari_libur','created_at','updated_at')->get(); 
        return Excel::create('gaji',  function($excel) use($gaji){
              $excel->sheet('mysheet',  function($sheet) use($gaji){
              $sheet->fromArray($gaji);
              });
        })->download('xls');
    }

    public function pdf()
    {
        $gaji = MasterGaji::all();
        $pdf = PDF::loadView('gaji.pdf',compact('gaji'),['gaji' => $gaji]);
        return $pdf->download('gaji.pdf');
    }
}
