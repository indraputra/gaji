<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\GajiKaryawan;
use App\MasterGaji;
use App\MasterKaryawan;
use App\Gaji;
use App\Karyawan;
use App\GajiPerBulan;
use Excel;
use DB;
use PDF;

class GajiKaryawanController extends Controller
{
    public function index()
    {
        $gajikaryawan = GajiKaryawan::
        join('master_gaji_karyawan','karyawan.id_gaji','=','master_gaji_karyawan.id_gaji')
        ->join('master_karyawans','karyawan.id_karyawan','=','master_karyawans.id_karyawan')
        ->select('master_gaji_karyawan.*','master_karyawans.*','karyawan.*')
        ->get();

        return view('gajikaryawan.index', compact('gajikaryawan'));
    }

    public function create()
    {
        $gaji = MasterGaji::all();
        $karyawan = MasterKaryawan::all();
        return view('gajikaryawan.create', compact('gaji','karyawan'));
    }

    public function store()
    {
        GajiKaryawan::create([
            'nama_karyawan' => request('nama_karyawan'),
            'id_gaji' => request('id_gaji'),
            'id_karyawan' => request('id_karyawan')
        ]);

        return redirect()->route('gajikaryawan.index');
    }

    public function edit($id)
    {
        $gajikaryawan = GajiKaryawan::where('id',$id)->join('master_gaji_karyawan','karyawan.id_gaji','=','master_gaji_karyawan.id_gaji')
        ->join('master_karyawans','karyawan.id_karyawan','=','master_karyawans.id_karyawan')
        ->select('master_gaji_karyawan.*','master_karyawans.*','karyawan.*')
        ->get();
        $gaji = MasterGaji::all();
        $karyawan = MasterKaryawan::all();

        return view('gajikaryawan.edit', compact('gajikaryawan','gaji','karyawan'));

    }

    public function update(Request $request,$id)
    {
        $gajikaryawan = GajiKaryawan::where('id', $id)->first();
        $gajikaryawan->id_karyawan = $request->id_karyawan;
        $gajikaryawan->id_gaji = $request->id_gaji;
        $gajikaryawan->save();

        return redirect()->route('gajikaryawan.index')->with('alert success','Data berhasil diubah!');
    }

    public function destroy($id)
    {
        $gajikaryawan = GajiKaryawan::where('id',$id)->first();
        $gajikaryawan->delete();
        return redirect()->route('gajikaryawan.index');
    }

    public function destroyall(Request $request)
    {
        $delid = $request->input('delid');
        GajiKaryawan::whereIn('id',$delid)->delete();
        return redirect()->route('gajikaryawan.index');
    }

    public function search(Request $request)
    {
      $search = $request->get('search');
      $result = GajiKaryawan::
      join('master_gaji_karyawan','karyawan.id_gaji','=','master_gaji_karyawan.id_gaji')
      ->join('master_karyawans','karyawan.id_karyawan','=','master_karyawans.id_karyawan')
      ->where('nama_karyawan','like','%'.$search.'%')
      ->orwhere('bagian','like','%'.$search.'%')
      ->orwhere('gaji_pokok','=',$search)
      ->get();
      return view('GajiKaryawan.result', compact('result'));
    }

    public function excel(){
        Excel::create('gajikaryawan', function($excel) {

        $excel->sheet('Sheet 1', function($sheet) {

            $gajikaryawan=DB::table('karyawan')
            ->join("master_karyawans","karyawan.id_karyawan","=","master_karyawans.id_karyawan")
            ->join("master_gaji_karyawan","karyawan.id_gaji","=","master_gaji_karyawan.id_gaji")
            ->select("karyawan.*","master_karyawans.*","master_gaji_karyawan.*")
            ->get();
                foreach($gajikaryawan as $gk) {
                 $data[] = array(
                    $gk->id,
                    $gk->nama_karyawan,
                    $gk->bagian,
                    $gk->tunjangan_jabatan,
                    $gk->tunjangan_transport,
                    $gk->tunjangan_kehadiran,
                    $gk->tunjangan_kesehatan,
                    $gk->tunjangan_komunikasi,
                    $gk->dl_dalam_kota,
                    $gk->dl_luar_kota,
                    $gk->lembur_hari_kerja,
                    $gk->lembur_hari_libur,
                    $gk->gaji_pokok,
                );
            }
            $headings = array('id', 'nama_karyawan', 'bagian', 'tunjangan_jabatan', 'tunjangan_transport','tunjangan_kehadiran','tunjangan_kesehatan','tunjangan_komunikasi','dl_dalam_kota','dl_luar_kota','lembur_hari_kerja','lembur_hari_libur','gaji_pokok');
            $sheet->prependRow(1, $headings);
            $sheet->fromArray($data, null, 'A1', false, false)->prependRow(1, $headings);
        });
    })->export('xls');
    }

    public function pdf()
    {
        $gajikaryawan = GajiKaryawan::
        join('master_gaji_karyawan','karyawan.id_gaji','=','master_gaji_karyawan.id_gaji')
        ->join('master_karyawans','karyawan.id_karyawan','=','master_karyawans.id_karyawan')
        ->select('master_gaji_karyawan.*','master_karyawans.*','karyawan.*')
        ->get();
        $pdf = PDF::loadView('gajikaryawan.pdf',compact('gajikaryawan'),['gajikaryawan'=>$gajikaryawan]);
        return $pdf->download('gajikaryawan.pdf');
    }

}
