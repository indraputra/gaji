<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGajiPerBulansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gaji_per_bulan', function (Blueprint $table) {
            $table->increments('id_gpb');
            $table->integer('id')->unsigned();
            $table->integer('total_tunjangan');
            $table->integer('total_dl');
            $table->integer('total_lembur');
            $table->integer('total_gaji');
            $table->integer('hari_masuk');
            $table->integer('total_jam_kerja');
            $table->integer('absen');
            $table->integer('izin');
            $table->integer('terlambat');
            $table->integer('cuti');
            $table->integer('dl_dalam_kota');
            $table->integer('dl_luar_kota');
            $table->integer('lembur_hari_kerja');
            $table->integer('lembur_hari_libur');
            $table->timestamps();

            $table->foreign('id')->references('id')->on('karyawan')->onDelete('CASCADE')->onUpdate('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('gaji_per_bulan');
    }
}
