<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateKaryawansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('karyawan', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_karyawan')->unsigned();
            $table->integer('id_gaji')->unsigned();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('id_karyawan')->references('id_karyawan')->on('master_karyawans')->onDelete('CASCADE')->onUpdate('CASCADE');
            $table->foreign('id_gaji')->references('id_gaji')->on('master_gaji_karyawan')->onDelete('CASCADE')->onUpdate('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('karyawan');
    }
}
