<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMasterGajisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('master_gaji_karyawan', function (Blueprint $table) {
            $table->increments('id_gaji');
            $table->string('bagian');
            $table->integer('tunjangan_jabatan');
            $table->integer('tunjangan_transport');
            $table->integer('tunjangan_kehadiran');
            $table->integer('tunjangan_kesehatan');
            $table->integer('tunjangan_komunikasi');
            $table->integer('gaji_pokok');
            $table->integer('dl_dalam_kota');
            $table->integer('dl_luar_kota');
            $table->integer('lembur_hari_kerja');
            $table->integer('lembur_hari_libur');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('master_gaji_karyawan');
    }
}
