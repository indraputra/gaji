@extends('desain.app')

@section('content')
<div class="panel-posisi">
    <div class="panel panel-default form-posisi">
      <div class="panel-heading">
        <div class="title"><center>Edit Gaji Karyawan</center></div>
      </div>
      <div class="panel-body">
        @foreach($gajikaryawan as $datas)
        <form action=" {{route('gajikaryawan.update' , $datas->id) }} " method="post">
        {{ csrf_field() }}
        {{method_field('PUT')}}

            <div class="form-group">
                <label for="">Nama Karyawan</label>
                <select name="id_karyawan" id="" class="form-control">
                  <option value="{{$datas->id_karyawan}}">{{$datas->nama_karyawan}}</option>
                    @foreach ($karyawan as $karyawan)
                        <option value="{{$karyawan->id_karyawan}}"> {{ $karyawan->nama_karyawan }} </option>
                    @endforeach
                </select>
            </div>

            <div class="form-group">
                <label for="">ID Gaji</label>
                <select name="id_gaji" id="" class="form-control">
                  <option value="{{$datas->bagian}}">{{$datas->bagian}}</option>
                    @foreach ($gaji as $gaji)
                        <option value="{{$gaji->id_gaji}}"> {{ $gaji->bagian }} </option>
                    @endforeach
                </select>
            </div>

            <div class="form-group">
                <input type="submit" value="Save" class="btn btn-primary" onClick="return confirm('Apakah anda yakin dengan data yang akan anda inputkan?')">
                <input type="reset" value="Reset" class="btn btn-primary" onClick="return confirm('Apakah anda yakin ingin mereset form inputan ini??')">
            </div>
            
        </form>
        @endforeach
      </div>
    </div>
</div>
@endsection