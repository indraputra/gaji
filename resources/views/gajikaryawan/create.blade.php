@extends('desain.app')

@section('content')
<div class="panel-posisi">
    <div class="panel panel-default form-posisi">
      <div class="panel-heading">
        <div class="title"><center>Input Gaji Karyawan</center></div>
      </div>
      <div class="panel-body">
        <form action="{{ route('gajikaryawan.store') }}" method="post" >
        {{ csrf_field() }}

            <div class="form-group">
                <label for="karyawan">Nama Karyawan</label>
                <select name="id_karyawan" id="karyawan" class="form-control" required autofocus>
                    <option></option>
                    @foreach ($karyawan as $karyawan)
                        <option id="karyawan" value="{{$karyawan->id_karyawan}}"> {{ $karyawan->nama_karyawan }} </option>
                    @endforeach
                </select>
            </div>

            <div class="form-group">
                <label for="">Bagian</label>
                <select name="id_gaji" id="" class="form-control" required autofocus>
                    <option></option>
                    @foreach ($gaji as $gaji)
                        <option value="{{$gaji->id_gaji}}"> {{ $gaji->bagian }} </option>
                    @endforeach
                </select>
            </div>

            <div class="form-group">
                 <input type="submit" value="Save" class="btn btn-primary" onClick="return confirm('Apakah anda yakin dengan data yang akan anda inputkan?')">
                <input type="reset" value="reset" class="btn btn-primary" onClick="return confirm('Apakah anda yakin akan mereset form ini?')">
            </div>

        </form>
      </div>
    </div>
</div>
@stop
