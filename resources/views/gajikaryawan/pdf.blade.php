<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Master Gaji</title>
    <link rel="stylesheet" href="">
    <style type="text/css" media="screen">
        table{
            border-collapse: collapse;
            margin-left: -35px;
        }
        td,th{
            border:1px solid;
        }
    </style>
</head>
<body>

    <center><h1>Gaji Karyawan</h1></center>
    <center><table>
        <thead>
            <tr>
                <th>No</th>
                <th>ID</th>
                <th>Nama</th>
                <th>Bagian</th>
                <th>Tunjangan Jabatan</th>
                <th>Tunjangan Transport</th>
                <th>Tunjangan Kehadiran</th>
                <th>Tunjangan Kesehatan</th>
                <th>Tunjangan Komunikasi</th>
                <th>DL Dalam Kota</th>
                <th>DL Luar Kota</th>
                <th>Lembur Hari Kerja</th>
                <th>Lembur Hari Libur</th>
                <th>Gaji Pokok</th>
            </tr>
        </thead>
        <tbody>
            <?php $no=1 ?>
            @foreach($gajikaryawan as $data)
            <?php $jabatan = number_format($data->tunjangan_jabatan,0,",",".") ?>
            <?php $transport = number_format($data->tunjangan_transport,0,",",".") ?>
            <?php $kehadiran = number_format($data->tunjangan_kehadiran,0,",",".") ?>
            <?php $kesehatan = number_format($data->tunjangan_kesehatan,0,",",".") ?>
            <?php $komunikasi = number_format($data->tunjangan_komunikasi,0,",",".") ?>
            <?php $dl_dalam_kota = number_format($data->dl_dalam_kota,0,",",".") ?>
            <?php $dl_luar_kota = number_format($data->dl_luar_kota,0,",",".") ?>
            <?php $lembur_hari_kerja = number_format($data->lembur_hari_kerja,0,",",".") ?>
            <?php $lembur_hari_libur = number_format($data->lembur_hari_libur,0,",",".") ?>
            <?php $gaji_pokok = number_format($data->gaji_pokok,0,",",".") ?>
            <tr>
                <td>{{ $no++ }}</td>
                <td>{{ $data->id}}</td>
                <td>{{ $data->nama_karyawan }}</td>
                <td>{{ $data->bagian }}</td>
                <td><?php echo"Rp$jabatan" ?></td>
                <td><?php echo"Rp$transport" ?></td>
                <td><?php echo"Rp$kehadiran" ?></td>
                <td><?php echo"Rp$kesehatan" ?></td>
                <td><?php echo"Rp$komunikasi" ?></td>
                <td><?php echo"Rp$dl_dalam_kota" ?></td>
                <td><?php echo"Rp$dl_luar_kota" ?></td>
                <td><?php echo"Rp$lembur_hari_kerja" ?></td>
                <td><?php echo"Rp$lembur_hari_libur" ?></td>
                <td><?php echo"Rp$gaji_pokok" ?></td>
            </tr>
            @endforeach
        </tbody>
    </table></center>
</body>
</html>