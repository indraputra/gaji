@extends('layout.app')

@section('title')
    Halaman Data Post
@stop

@section('body')
    <h1>Belajar Laravel Sampai Bisa</h1>
        @if(count($post) > 0)
            <ul>
                @foreach($post as $post)
                    <a href="{{ route('post.show',$post['id']) }}"><li>{{ $post['title'] }}</li></a>
                @endforeach
            </ul>
            @else
            <p>Data tidak tercantum</p>
        @endif
@stop
