<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>App Gaji</title>
	<link rel="stylesheet" href="{!! asset('assets2/css/style.css') !!}" media="screen">
	<link rel="stylesheet" href="{!! asset('assets2/css/bootstrap.min.css') !!}" media="screen">
</head>
<body>
	<main>
	<div class="kiri">
		<center class="menu">MENU</center>
		<hr class="hr2">
		<nav class="navbar">
			<ul>
				<li><a href="{{route('home')}}" class="glyphicon glyphicon-home space"> Home</a></li>
				<li><a href="{{route('karyawan.index')}}" class="glyphicon glyphicon-list-alt space" > Tabel Master Karyawan</a></li>
				<li><a href="{{route('gaji.index')}}" class="glyphicon glyphicon-list-alt space" > Tabel Master Gaji</a></li>
				<li><a href="{{route('gajikaryawan.index')}}" class="glyphicon glyphicon-list-alt space"> Tabel Gaji Karyawan</a></li>
				<li><a href="{{route('karyawan.create')}}" class="glyphicon glyphicon-pencil space"> Input Karyawan</a></li>
				<li><a href="{{route('gaji.create')}}" class="glyphicon glyphicon-pencil space"> Input Gaji</a></li>
				<li><a href="{{route('gajikaryawan.create')}}" class="glyphicon glyphicon-pencil space"> Input Gaji Karyawan</a></li>
				<li><a href="" class="glyphicon glyphicon-user space"> {{ Auth::user()->name }}</a></li>
				<li>
                    <a href="{{ route('logout') }}" class="glyphicon glyphicon-off space" 
                    	onclick="event.preventDefault();
                	    document.getElementById('logout-form').submit();">
                        Logout
                    </a>

                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        {{ csrf_field() }}
                    </form>
                </li>
			</ul>
		</nav>
	</div>

	<div class="kanan">
		@yield('content')
		
	</div>
	</main>

	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
    <script src="{{ asset('js/app.js') }}"></script>

	<script type="text/javascript">
      function addForm(){
        save_method="add";
        $('input[name=_method]').val('POST');
        $('#create-modal').modal('show');
        $('#create-modal form')[0].reset();
        $('.modal-title').text('Absen Per Bulan');
      }
    </script>
</body>
</html>