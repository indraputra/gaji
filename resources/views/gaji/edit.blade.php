@extends('desain.app')

@section('content')
<div class="panel-posisi">
    <div class="panel panel-default form-posisi">
      <div class="panel-heading">
        <div class="title"><center>Edit Gaji</center></div>
      </div>
      <div class="panel-body">
        @foreach($gaji as $datas)
        <form action=" {{route('gaji.update' , $datas->id_gaji) }} " method="post">
        {{ csrf_field() }}
        {{method_field('PUT')}}

            <div class="form-group">
                <label for="">Bagian</label>
                <input type="text" class="form-control" name="bagian" value="{{$datas->bagian}}" > 
            </div>

            <div class="form-group">
                <label for="">Tunjangan Jabatan</label>
                <input type="text" class="form-control" name="tunjangan_jabatan" value="{{$datas->tunjangan_jabatan}}" > 
            </div>

            <div class="form-group">
                <label for="">Tunjangan Transport</label>
                <input type="text" class="form-control" name="tunjangan_transport" value="{{$datas->tunjangan_transport}}"> 
            </div>

            <div class="form-group">
                <label for="">Tunjangan Kehadiran</label>
                <input type="text" class="form-control" name="tunjangan_kehadiran" value="{{$datas->tunjangan_kehadiran}}"> 
            </div>

            <div class="form-group">
                <label for="">Tunjangan Kesehatan</label>
                <input type="text" class="form-control" name="tunjangan_kesehatan" value="{{$datas->tunjangan_kesehatan}}"> 
            </div>

            <div class="form-group">
                <label for="">Tunjangan Komunikasi</label>
                <input type="text" class="form-control" name="tunjangan_komunikasi" value="{{$datas->tunjangan_komunikasi}}"> 
            </div>

            <div class="form-group">
                <label for="">Dl Dalam Kota</label>
                <input type="text" class="form-control" name="dl_dalam_kota" value="{{$datas->dl_dalam_kota}}"> 
            </div>

            <div class="form-group">
                <label for="">DL Luar Kota</label>
                <input type="text" class="form-control" name="dl_luar_kota" value="{{$datas->dl_luar_kota}}"> 
            </div>

            <div class="form-group">
                <label for="">Lembur Hari Kerja</label>
                <input type="text" class="form-control" name="lembur_hari_kerja" value="{{$datas->lembur_hari_kerja}}"> 
            </div>

            <div class="form-group">
                <label for="">Lembur Hari Libur</label>
                <input type="text" class="form-control" name="lembur_hari_libur" value="{{$datas->lembur_hari_libur}}"> 
            </div>

            <div class="form-group">
                <label for="">Gaji Pokok</label>
                <input type="text" class="form-control" name="gaji_pokok" value="{{$datas->gaji_pokok}}"> 
            </div>

            <div class="form-group">
                <input type="submit" value="Save" class="btn btn-primary">
            </div>
            
        </form>
        @endforeach
      </div>
    </div>
</div>
@endsection