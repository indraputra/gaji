@extends('desain.app')

@section('content')
<div class="panel-posisi">
    <div class="panel panel-default form-posisi">
      <div class="panel-heading">
        <div class="title"><center>Input Gaji</center></div>
      </div>
      <div class="panel-body">
        <form action="{{ route('gaji.store') }}" method="post" >
        {{ csrf_field() }}

            <div class="form-group">
                <label for="">Bagian</label>
                <input type="text" class="form-control" name="bagian" placeholder="Masukan data" required>
            </div>

            <div class="form-group">
                <label for="">Tunjangan Jabatan</label>
                <input type="number" class="form-control" name="tunjangan_jabatan" placeholder="Masukan Nominal"  min="0"  required>
            </div>

            <div class="form-group">
                <label for="">Tunjangan Transport</label>
                <input type="number" class="form-control" name="tunjangan_transport" placeholder="Masukan Nominal" min="0" required>
            </div>

            <div class="form-group">
                <label for="">Tunjangan Kehadiran</label>
                <input type="number" class="form-control" name="tunjangan_kehadiran" placeholder="Masukan Nominal" min ="0" required>
            </div>

            <div class="form-group">
                <label for="">Tunjangan Kesehatan</label>
                <input type="number" class="form-control" name="tunjangan_kesehatan" placeholder="Masukan Nominal" min="0" required>
            </div>

            <div class="form-group">
                <label for="">Tunjangan Komunikasi</label>
                <input type="number" class="form-control" name="tunjangan_komunikasi" placeholder="Masukan Nominal" min="0" required>
            </div>

            <div class="form-group">
                <label for="">DL Dalam Kota</label>
                <input type="number" class="form-control" name="dl_dalam_kota" placeholder="Masukan Nominal" min="0" required>
            </div>

            <div class="form-group">
                <label for="">DL Luar Kota</label>
                <input type="number" class="form-control" name="dl_luar_kota" placeholder="Masukan Nominal" min="0" required>
            </div>

            <div class="form-group">
                <label for="">Lembur Hari Kerja</label>
                <input type="number" class="form-control" name="lembur_hari_kerja" placeholder="Masukan Nominal" min="0" required>
            </div>

            <div class="form-group">
                <label for="">Lembur Hari Libur</label>
                <input type="number" class="form-control" name="lembur_hari_libur" placeholder="Masukan Nominal" min="0" required>
            </div>

            <div class="form-group">
                <label for="">Gaji Pokok</label>
                <input type="number" class="form-control" name="gaji_pokok" placeholder="Masukan Nominal" min="0" required>
            </div>

            <div class="form-group">
                 <input type="submit" value="Save" class="btn btn-primary" onClick="return confirm('Apakah anda yakin dengan data yang akan anda inputkan?')">
                <input type="reset" value="reset" class="btn btn-primary" onClick="return confirm('Apakah anda yakin akan mereset form ini?')">
            </div>

        </form>
      </div>
    </div>
</div>

@stop
