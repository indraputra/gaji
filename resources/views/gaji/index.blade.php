@extends('desain.app')

@section('content')
<div class="">
    <div class="panel panel-default form-posisi">
      <div class="panel-heading">
        <div class="title"><center>Tabel Gaji</center></div>
      </div>
      <div class="panel-body">
        <form action="{{ route('gaji.search') }}" method="get">
          <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col">
                      <input type="text" name="search" class="form-control" value="" placeholder="Cari bagian atau gaji" required>
                    </div>
                    <div class="col">
                        <div class="pull pull-left">
                            <button type="submit" class="btn btn-outline-primary  glyphicon glyphicon-search space" name="button"> Cari</button>    
                        </div>
                        
                        <div class="pull pull-left excel">
                            <a href="{{route('gaji.excel')}}" class="btn btn-success glyphicon glyphicon-save space" title="">
                                Export to excel
                            </a>
                        </div>

                        <div class="pull pull-left excel">
                            <a href="{{route('gaji.pdf')}}" class="btn btn-danger glyphicon glyphicon-save space" title="">
                                Export to pdf
                            </a>
                        </div>
                    </div>
                </div>
            </div>
          </div>
        </form>
        <form action="{{route('gaji.destroyall')}}" method="post">
        {{ csrf_field() }}
        <table class="table table-hover posisi-tabel">
                <thead>
                    <th>#</th>
                    <th>No</th>
                    <th>Bagian</th>
                    <th>Tunjangan Jabatan</th>
                    <th>Tunjangan Transport</th>
                    <th>Tunjangan Kehadiran</th>
                    <th>Tunjangan Kesehatan</th>
                    <th>Tunjangan Komunikasi</th>
                    <th>DL Dalam Kota</th>
                    <th>DL Luar Kota</th>
                    <th>Lembur Hari Kerja</th>
                    <th>Lembur Hari Libur</th>
                    <th>Gaji Pokok</th>
                    <th>Opsi</th>
                </thead>
                <tbody>
                <?php $no=1?>
                @foreach($gaji as $result=>$gaji)
                <?php $jabatan = number_format($gaji->tunjangan_jabatan,0,",",".") ?>
                <?php $transport = number_format($gaji->tunjangan_transport,0,",",".") ?>
                <?php $kehadiran = number_format($gaji->tunjangan_kehadiran,0,",",".") ?>
                <?php $kesehatan = number_format($gaji->tunjangan_kesehatan,0,",",".") ?>
                <?php $komunikasi = number_format($gaji->tunjangan_komunikasi,0,",",".") ?>
                <?php $dl_dalam_kota = number_format($gaji->dl_dalam_kota,0,",",".") ?>
                <?php $dl_luar_kota = number_format($gaji->dl_luar_kota,0,",",".") ?>
                <?php $lembur_hari_kerja = number_format($gaji->lembur_hari_kerja,0,",",".") ?>
                <?php $lembur_hari_libur = number_format($gaji->lembur_hari_libur,0,",",".") ?>
                <?php $gaji_pokok = number_format($gaji->gaji_pokok,0,",",".") ?>
                    <tr id="tr_{{$gaji->id_gaji}}">
                        <td><input type="checkbox" name="delid[]" value="{{$gaji->id_gaji}}"></td>
                        <td width="10px">{{$no++}}</td>
                        <td>{{ $gaji->bagian }}</td>
                        <td><?php echo"Rp$jabatan" ?></td>
                        <td><?php echo"Rp$transport" ?></td>
                        <td><?php echo"Rp$kehadiran" ?></td>
                        <td><?php echo"Rp$kesehatan" ?></td>
                        <td><?php echo"Rp$komunikasi" ?></td>
                        <td><?php echo"Rp$dl_dalam_kota" ?></td>
                        <td><?php echo"Rp$dl_luar_kota" ?></td>
                        <td><?php echo"Rp$lembur_hari_kerja" ?></td>
                        <td><?php echo"Rp$lembur_hari_libur" ?></td>
                        <td><?php echo"Rp$gaji_pokok" ?></td>
                        <td>
                            <div class="pull-left atur">
                                <a href="{{ route('gaji.edit',$gaji->id_gaji) }}">
                                    <button type="button" class="btn btn-success btn-sm" onClick="return confirm('Apakah anda yakin mengedit data ini??')">
                                    <div class="glyphicon glyphicon-edit">
                                        Edit
                                    </div>
                                    </button>
                                </a>
                            </div>

                            <div class="pull-left atur">
                                <form action="{{ route('gaji.destroy', $gaji->id_gaji) }}" method="post">
                                    {{csrf_field()}}
                                    {{method_field('DELETE')}}
                                    <button type="submit" class="btn btn-sm btn-danger" onClick="return confirm('Apakah anda yakin menghapus data ini??')">
                                        <div class="glyphicon glyphicon-trash">
                                            Hapus
                                        </div>
                                    </button>
                                </form>
                            </div>

                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            <button type="submit" class="btn btn-danger glyphicon glyphicon-trash space" onClick="return confirm('Apakah anda yakin ingin menghapus semua data yang anda pilih?')">Hapus terpilih</button>
            </form>
      </div>
    </div>
</div>



@endsection

