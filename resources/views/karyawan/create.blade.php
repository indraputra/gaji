@extends('desain.app')

@section('content')
<div class="panel-posisi">
    <div class="panel panel-default form-posisi">
      <div class="panel-heading">
        <div class="title"><center>Input Karyawan</center></div>
      </div>
      <div class="panel-body">
        <form action="{{ route('karyawan.store') }}" method="post" >
        {{ csrf_field() }}

            <div class="form-group">
                <label for="">NIP</label>
                <input type="text" class="form-control" name="nip" placeholder="Masukan NIP" required oninvalid="this.setCustomValidity('NIP tidak boleh kosong')" oninput="setCustomValidity('')">
            </div>

            <div class="form-group">
                <label for="">Nama Karyawan</label>
                <input type="text" class="form-control" name="nama_karyawan" placeholder="Masukan Nama Karyawan" required oninvalid="this.setCustomValidity('Nama karyawan tidak boleh kosong')" oninput="setCustomValidity('')">
            </div>

            <div class="form-group">
                <label for="">Jabatan</label>
                <input type="text" class="form-control" name="jabatan" placeholder="Masukan Jabatan" required oninvalid="this.setCustomValidity('Jabatan tidak boleh kosong')" oninput="setCustomValidity('')">
            </div>

            <div class="form-group">
                <label for="">Fungsional</label>
                <input type="text" class="form-control" name="fungsional" placeholder="Masukan Fungsional" required
                oninvalid="this.setCustomValidity('Fungsional tidak boleh kosong')" oninput="setCustomValidity('')">
            </div>

            <div class="form-group">
                <input type="submit" value="Save" class="btn btn-primary" onClick="return confirm('Apakah anda yakin dengan data yang akan anda inputkan?')">
                <input type="reset" value="reset" class="btn btn-primary" onClick="return confirm('Apakah anda yakin akan mereset form ini?')">
            </div>

        </form>
      </div>
    </div>
</div>
@stop
