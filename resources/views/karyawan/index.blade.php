@extends('desain.app')

@section('content')

<div class="panel-posisi">
    <div class="panel panel-default form-posisi">
      <div class="panel-heading">
        <div class="title"><center>Tabel Karyawan</center></div>
      </div>
      <div class="panel-body">
        <form action="{{ route('karyawan.search') }}" method="get">
          <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col">
                      <input type="text" name="search" class="form-control" value="" placeholder="Cari nama karyawan, nip , jabatan ,fungsional" required>

                    </div>
                    <div class="col">
                        <div class="pull pull-left">
                            <button type="submit" class="btn btn-outline-primary  glyphicon glyphicon-search space" name="button"> Cari</button>    
                        </div>
                        
                        <div class="pull pull-left excel">
                            <a href="{{route('karyawan.excel')}}" class="btn btn-success glyphicon glyphicon-save space" title="">
                                Export to excel
                            </a>
                        </div>

                        <div class="pull pull-left excel">
                            <a href="{{route('karyawan.pdf')}}" class="btn btn-danger glyphicon glyphicon-save space" title="">
                                Export to pdf
                            </a>
                        </div>
                    </div>
                </div>
            </div>
          </div>
        </form>



        <form action="{{ route('karyawan.destroyall') }}" method="post">
        {{ csrf_field() }}
        <table class="table table-hover">
                <thead >
                    <th>#</th>
                    <th>No</th>
                    <th>Nama Karyawan</th>
                    <th>NIP</th>
                    <th>Jabatan</th>
                    <th>Fungsional</th>
                    <th>Opsi</th>
                </thead>
                <tbody>
                <?php $no=1?>
                @foreach($karyawan as $result=>$karyawan)
                    <tr>
                        <td hidden>{{ $karyawan->id_karyawan}}</td>
                        <td><input type="checkbox" name="delid[]" value="{{ $karyawan->id_karyawan }}"> </td>
                        <td>{{$no++}}</td>
                        <td>{{ $karyawan->nama_karyawan}}</td>
                        <td>{{ $karyawan->nip}}</td>
                        <td>{{ $karyawan->jabatan}}</td>
                        <td>{{ $karyawan->fungsional}}</td>
                        <td>

                            <div class="pull-left atur">
                                <a href="{{ route('karyawan.edit',$karyawan->id_karyawan) }}">
                                    <button type="button" class="btn btn-success btn-sm" onClick="return confirm('Apakah anda yakin mengedit data ini??')">
                                    <div class="glyphicon glyphicon-edit">
                                        Edit
                                    </div>
                                    </button>
                                </a>
                            </div>


                            <div class="pull-left atur">
                                <form action="{{ route('karyawan.destroy', $karyawan->id_karyawan) }}" method="post">
                                    {{csrf_field()}}
                                    {{method_field('DELETE')}}
                                    <button type="submit" class="btn btn-sm btn-danger" onClick="return confirm('Apakah anda yakin menghapus data ini??')">
                                        <div class="glyphicon glyphicon-trash">
                                            Hapus
                                        </div>
                                    </button>
                                </form>
                            </div>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            <button type="submit" class="btn btn-danger glyphicon glyphicon-trash space" onClick="return confirm('Apakah anda yakin ingin menghapus semua data yang anda pilih?')"> Hapus Terpilih</button>
            </form>
      </div>
    </div>
</div>

@endsection
