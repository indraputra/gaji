@extends('desain.app')

@section('content')

@if(count($result))
<div class="panel-posisi">
    <div class="panel panel-default form-posisi">
      <div class="panel-heading">
        <div class="title"><center>Tabel Karyawan</center></div>
      </div>
      <div class="panel-body">
        <form action="{{ route('karyawan.search') }}" method="get">
          <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col">
                      <input type="text" name="search" class="form-control" value="" placeholder="Cari nama karyawan, nip , jabatan ,fungsional">

                    </div>
                    <div class="col">
                        <div class="pull pull-left">
                            <button type="submit" class="btn btn-outline-primary  glyphicon glyphicon-search space" name="button"> Cari</button>    
                        </div>
                        
                        <div class="pull pull-left excel">
                            <a href="{{route('karyawan.excel')}}" class="btn btn-success glyphicon glyphicon-save space" title="">
                                Export to excel
                            </a>
                        </div>

                        <div class="pull pull-left excel">
                            <a href="{{route('karyawan.pdf')}}" class="btn btn-danger glyphicon glyphicon-save space" title="">
                                Export to pdf
                            </a>
                        </div>
                    </div>
                </div>
            </div>
          </div>
        </form>
        <table class="table table-hover">
                <thead >
                    <th>No</th>
                    <th>Nama Karyawan</th>
                    <th>NIP</th>
                    <th>Jabatan</th>
                    <th>Fungsional</th>
                    <th>Opsi</th>
                </thead>
                <tbody>
                <?php $no=1?>
                @foreach($result as $result)
                    <tr>
                        <td>{{$no++}}</td>
                        <td>{{ $result->nama_karyawan}}</td>
                        <td>{{ $result->nip}}</td>
                        <td>{{ $result->jabatan}}</td>
                        <td>{{ $result->fungsional}}</td>
                        <td>

                            <div class="pull-left atur">
                                <a href="{{ route('karyawan.edit',$result->id_karyawan) }}">
                                    <button type="button" class="btn btn-success btn-sm" onClick="return confirm('Apakah anda yakin mengedit data ini??')">
                                    <div class="glyphicon glyphicon-edit">
                                        Edit
                                    </div>
                                    </button>
                                </a>
                            </div>


                            <div class="pull-left atur">
                                <form action="{{ route('karyawan.destroy', $result->id_karyawan) }}" method="post">
                                    {{csrf_field()}}
                                    {{method_field('DELETE')}}
                                    <button type="submit" class="btn btn-sm btn-danger" onClick="return confirm('Apakah anda yakin menghapus data ini??')">
                                        <div class="glyphicon glyphicon-trash">
                                            Hapus
                                        </div>
                                    </button>
                                </form>
                            </div>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
      </div>
    </div>
</div>
@else

<div class="container">
  <div class="alert alert-danger">Not Found !!!</div>
</div>
@endif


@endsection
