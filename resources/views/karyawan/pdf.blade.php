<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Master Karyawan</title>
	<link rel="stylesheet" href="">
	<style type="text/css" media="screen">
		table{
			border-collapse: collapse;
		}
		td,th{
			border:1px solid;
		}
	</style>
</head>
<body>
	<center><h1>Master Karyawan</h1></center>
	<center><table>
		<thead>
			<tr>
				<th>No</th>
				<th>ID</th>
				<th>Nama</th>
				<th>NIP</th>
				<th>Jabatan</th>
				<th>Fungsional</th>
			</tr>
		</thead>
		<tbody>
			<?php $no=1 ?>
			@foreach($karyawan as $data)
			<tr>
				<td>{{ $no++ }}</td>
				<td>{{ $data->id_karyawan }}</td>
				<td>{{ $data->nama_karyawan }}</td>
				<td>{{ $data->nip }}</td>
				<td>{{ $data->jabatan }}</td>
				<td>{{ $data->fungsional }}</td>
			</tr>
			@endforeach
		</tbody>
	</table></center>
</body>
</html>