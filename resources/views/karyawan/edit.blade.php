@extends('desain.app')

@section('content')
<div class="panel-posisi">
    <div class="panel panel-default form-posisi">
      <div class="panel-heading">
        <div class="title"><center>Edit Karyawan</center></div>
      </div>
      <div class="panel-body">
        @foreach($karyawan as $datas)
        <form action=" {{route('karyawan.update' , $datas->id_karyawan) }} " method="post">
        {{ csrf_field() }}
        {{method_field('PUT')}}

            <div class="form-group">
                <label for="">NIP</label>
                <input type="text" class="form-control" name="nip" value="{{$datas->nip}}" > 
            </div>

            <div class="form-group">
                <label for="">Nama Karyawan</label>
                <input type="text" class="form-control" name="nama_karyawan" value="{{$datas->nama_karyawan}}"> 
            </div>

            <div class="form-group">
                <label for="">Jabatan</label>
                <input type="text" class="form-control" name="jabatan" value="{{$datas->jabatan}}"> 
            </div>

            <div class="form-group">
                <label for="">Fungsional</label>
                <input type="text" class="form-control" name="fungsional" value="{{$datas->fungsional}}"> 
            </div>

            <div class="form-group">
                <input type="submit" value="Save" class="btn btn-primary" onClick="return confirm('Apakah anda yakin dengan data yang akan anda inputkan?')">
                <input type="reset" value="Reset" class="btn btn-primary" onClick="return confirm('Apakah anda yakin ingin mereset form inputan ini??')">
            </div>
            
        </form>
        @endforeach
      </div>
    </div>
</div>
@endsection