@extends('desain.app')

@section('content')
<div class="panel-posisi">
    <div class="panel panel-default form-posisi">
      <div class="panel-heading">
        <div class="title"><center>Edit Gaji</center></div>
      </div>
      <div class="panel-body">
        @foreach($absen as $datas)
            <fieldset disabled>
            <div class="form-group">
                <label for="">Hari Masuk</label>
                <input type="text" class="form-control" name="" value="{{$datas->hari_masuk}} hari" > 
            </div>

            <div class="form-group">
                <label for="">Total Jam Kerja</label>
                <input type="text" class="form-control" name="" value="{{$datas->total_jam_kerja}} jam" > 
            </div>

            <div class="form-group">
                <label for="">Absen</label>
                <input type="text" class="form-control" name="" value="{{$datas->absen}} hari"> 
            </div>

            <div class="form-group">
                <label for="">Izin</label>
                <input type="text" class="form-control" name="" value="{{$datas->izin}} hari"> 
            </div>

            <div class="form-group">
                <label for="">Terlambat</label>
                <input type="text" class="form-control" name="" value="{{$datas->terlambat}}x"> 
            </div>

            <div class="form-group">
                <label for="">Cuti</label>
                <input type="text" class="form-control" name="" value="{{$datas->cuti}}x"> 
            </div>

            <div class="form-group">
                <label for="">Dl Dalam Kota</label>
                <input type="text" class="form-control" name="" value="{{$datas->dl_dalam_kota}}x"> 
            </div>

            <div class="form-group">
                <label for="">Dl Luar Kota</label>
                <input type="text" class="form-control" name="" value="{{$datas->dl_luar_kota}}x"> 
            </div>

            <div class="form-group">
                <label for="">Lembur Hari Kerja</label>
                <input type="text" class="form-control" name="" value="{{$datas->lembur_hari_kerja}}x"> 
            </div>

            <div class="form-group">
                <label for="">Lembur Hari Libur</label>
                <input type="text" class="form-control" name="" value="{{$datas->lembur_hari_libur}}x"> 
            </div>
            </fieldset>

        @endforeach
      </div>
    </div>
</div>
@endsection