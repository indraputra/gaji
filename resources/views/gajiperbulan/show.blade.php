@extends('desain.app')

@section('content')
<div class="panel-posisi">
    <div class="panel panel-default form-posisi">
      <div class="panel-heading">
        <div class="title"><center>Tabel Gaji Perbulan</center></div>
      </div>
      <div class="panel-body">
        
          <div class="card">
            <div class="card-body">
                <div class="row"> 
                    @foreach($gajiperbulan as $gpb)
                    <form action="{{ route('gajiperbulan.search',$gpb->id) }}" method="get">
                    @endforeach
                    <div class="col">
                      <input type="date" name="search" class="form-control" value="" placeholder="Cari total gaji" required>
                    </div>
                    <div class="col">
                        <div class="pull pull-left">
                            <button type="submit" class="btn btn-outline-primary  glyphicon glyphicon-search space" name="button"> Cari</button>    
                        </div>
                        
                        <div class="pull pull-left excel">
                            <a href="{{route('gajiperbulan.excel')}}" class="btn btn-success glyphicon glyphicon-save space" title="">
                                Export to excel
                            </a>
                        </div>
                        
                    </div>    
                    </form>
                    @foreach($exportpdf as $t)
                    <form action="{{route('gajiperbulan.pdf', $t->id)}}" method="get" accept-charset="utf-8">@endforeach
                        <button type="" class="btn btn-danger glyphicon glyphicon-save space"> Export to pdf</button>
                    </form>
                </div>
            </div>
          </div>
               
        <table class="table table-hover">
                <thead>
                    <th>No</th>
                    <th>Total Tunjangan</th>
                    <th>Dinas Luar</th>
                    <th>Lembur</th>
                    <th>Total Gaji</th>
                    <th>Tanggal</th>
                    <th>Opsi</th>
                </thead>
                <tbody >
                <?php $no=1?>
                @foreach($gajiperbulan as $gpb)
                <?php $total_tunjangan = number_format($gpb->total_tunjangan,0,",",".") ?>
                <?php $total_dl = number_format($gpb->total_dl,0,",",".") ?>
                <?php $total_lembur = number_format($gpb->total_lembur,0,",",".") ?>
                <?php $total_gaji = number_format($gpb->total_gaji,0,",",".") ?>
                    <tr>
                        <td>{{$no++}}</td>
                        <td><?php echo"Rp $total_tunjangan" ?></td>
                        <td><?php echo"Rp $total_dl" ?></td>
                        <td><?php echo"Rp $total_lembur" ?></td>
                        <td><?php echo"Rp $total_gaji" ?></td>
                        <td>{{$gpb->created_at}}</td>
                        <td>
                            <div class="pull-left atur">
                                <a href="{{ route('gajiperbulan.absen',$gpb->id_gpb) }}">                
                                    <button type="button" class="btn btn-primary btn-sm">
                                    <div class="glyphicon glyphicon-eye-open space">
                                        Absen
                                    </div>
                                    </button>
                                </a>    
                            </div>
                            <div class="pull-left atur">
                                <a onclick="addForm('.$gpb->id_pgb.')" class="btn btn-info btn-sm">
                                    <i class="glyphicon glyphicon-eye-open space"> Absen All</i>
                                </a>     
                            </div>
                            
                            <div class="pull-left atur">
                                <form action="{{ route('gajiperbulan.destroy', $gpb->id_gpb) }}" method="post">
                                    {{csrf_field()}}
                                    {{method_field('DELETE')}}
                                    <button type="submit" class="btn btn-sm btn-danger" onClick="return confirm('Apakah anda yakin menghapus data ini??')">
                                        <div class="glyphicon glyphicon-trash">
                                            Hapus
                                        </div>
                                    </button>
                                </form>
                            </div>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
      </div>
    </div>
</div>
@include('gajiperbulan.form')
@stop
