@extends('desain.app')

@section('content')

<div class="panel-posisi">
    <div class="panel panel-default form-posisi">
      <div class="panel-heading">
        <div class="title"><center>Tambah Gaji</center></div>
      </div>
      <div class="panel-body">
        @foreach($gajikaryawan as $datas)
        <form action="{{route('gajiperbulan.store')}}" method="post">
        {{ csrf_field() }}

            <b><table class="custom-table">
                <tr>
                    <td>Nama</td>
                    <td>:</td>
                    <td><?php echo "$datas->nama_karyawan" ?></td>
                </tr>

                <tr>
                    <td>NIP</td>
                    <td>:</td>
                    <td><?php echo "$datas->nip" ?></td>
                </tr>

                <tr>
                    <td>Bagian</td>
                    <td>:</td>
                    <td><?php echo "$datas->bagian" ?></td>
                </tr>

                <tr>
                    <td>Jabatan</td>
                    <td>:</td>
                    <td><?php echo "$datas->jabatan" ?></td>
                </tr>

                <tr>
                    <td>Fungsional</td>
                    <td>:</td>
                    <td><?php echo "$datas->fungsional" ?></td>
                </tr>
            </table></b>

            <hr>
            <input type="hidden" name="id" value="{{$datas->id}}">
            <input type="hidden" name="t1" value="{{$datas->tunjangan_jabatan}}">
            <input type="hidden" name="t2" value="{{$datas->tunjangan_transport}}">
            <input type="hidden" name="t3" value="{{$datas->tunjangan_kehadiran}}">
            <input type="hidden" name="t4" value="{{$datas->tunjangan_kesehatan}}">
            <input type="hidden" name="t5" value="{{$datas->tunjangan_komunikasi}}">
            <input type="hidden" name="ddk" value="{{$datas->dl_dalam_kota}}">
            <input type="hidden" name="dlk" value="{{$datas->dl_luar_kota}}">
            <input type="hidden" name="lhk" value="{{$datas->lembur_hari_kerja}}">
            <input type="hidden" name="lhl" value="{{$datas->lembur_hari_libur}}">

            <fieldset disabled>
            <div class="form-group">
                <label for="">Tunjangan Jabatan</label>
                <input type="text" class="form-control" name="t1" value="{{$datas->tunjangan_jabatan}}" > 
            </div>

            <div class="form-group">
                <label for="">Tunjangan Transport</label>
                <input type="text" class="form-control" name="t2" value="{{$datas->tunjangan_transport}}"> 
            </div>

            <div class="form-group">
                <label for="">Tunjangan Kehadiran</label>
                <input type="text" class="form-control" name="t3" value="{{$datas->tunjangan_kehadiran}}"> 
            </div>

            <div class="form-group">
                <label for="">Tunjangan Kesehatan</label>
                <input type="text" class="form-control" name="t4" value="{{$datas->tunjangan_kesehatan}}"> 
            </div>

            <div class="form-group">
                <label for="">Tunjangan Komunikasi</label>
                <input type="text" class="form-control" name="t5" value="{{$datas->tunjangan_komunikasi}}"> 
            </div>

            <div class="form-group">
                <label for="">Gaji Pokok</label>
                <input type="text" class="form-control" name="gaji_pokok" value="{{$datas->gaji_pokok}}"> 
            </div>

            <div class="form-group">
                <label for="">DL Dalam Kota</label>
                <input type="text" class="form-control" name="ddk" value="{{$datas->dl_dalam_kota}}"> 
            </div>

            <div class="form-group">
                <label for="">DL Luar Kota</label>
                <input type="text" class="form-control" name="dlk" value="{{$datas->dl_luar_kota}}"> 
            </div>

            <div class="form-group">
                <label for="">Lembur Hari Kerja</label>
                <input type="text" class="form-control" name="lhk" value="{{$datas->lembur_hari_kerja}}"> 
            </div>

            <div class="form-group">
                <label for="">Lembur Hari Libur</label>
                <input type="text" class="form-control" name="lhl" value="{{$datas->lembur_hari_libur}}"> 
            </div>
            </fieldset>

            <div class="form-group">
                <label for="">Hari Masuk</label>
                <input type="text" class="form-control" name="hari_masuk" placeholder="Masukan Data"> 
            </div>

            <div class="form-group">
                <label for="">Total Jam Kerja</label>
                <input type="text" class="form-control" name="total_jam_kerja" placeholder="Masukan Data"> 
            </div>

            <div class="form-group">
                <label for="">Absen</label>
                <input type="text" class="form-control" name="absen" placeholder="Masukan Data"> 
            </div>

            <div class="form-group">
                <label for="">Izin</label>
                <input type="text" class="form-control" name="izin" placeholder="Masukan Data"> 
            </div>

            <div class="form-group">
                <label for="">Terlambat</label>
                <input type="text" class="form-control" name="terlambat" placeholder="Masukan Data"> 
            </div>

            <div class="form-group">
                <label for="">Cuti</label>
                <input type="text" class="form-control" name="cuti" placeholder="Masukan Data"> 
            </div>

            <div class="form-group">
                <label for="">DL Dalam Kota</label>
                <input type="text" class="form-control" name="dl_dalam_kota" placeholder="Masukan Data"> 
            </div>

            <div class="form-group">
                <label for="">DL Luar Kota</label>
                <input type="text" class="form-control" name="dl_luar_kota" placeholder="Masukan Data"> 
            </div>

            <div class="form-group">
                <label for="">Lembur Hari Kerja</label>
                <input type="text" class="form-control" name="lembur_hari_kerja" placeholder="Masukan Data"> 
            </div>

            <div class="form-group">
                <label for="">Lembur Hari Libur</label>
                <input type="text" class="form-control" name="lembur_hari_libur" placeholder="Masukan Data"> 
            </div>

            <div class="form-group">
                <input type="submit" value="Save" class="btn btn-primary" onClick="return confirm('Apakah anda yakin dengan data yang akan diinputkan?')">
                <input type="reset" value="Reset" class="btn btn-primary" onClick="return confirm('Apakaha anda yakin akan mereset form inputan ini??')">
            </div>

        </form>
        @endforeach
      </div>
    </div>
</div>

@stop