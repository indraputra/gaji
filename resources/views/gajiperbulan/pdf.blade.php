<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Gaji Per Bulan</title>
	<link rel="stylesheet" href="">
	<style type="text/css" media="screen">
		table{
			border-collapse: collapse;
		}
		td,th{
			border:1px solid;
		}
	</style>
</head>
<body>
	<center><h1>Gaji Per Bulan</h1></center>
	<center><table>
		<thead>
			<tr>
				<th>No</th>
				<th>ID</th>
				<th>ID Gaji</th>
				<th>Total Tunjangan</th>
				<th>Total Dinas</th>
				<th>Total Lembur</th>
				<th>Total Gaji</th>
				<th>Tanggal</th>
			</tr>
		</thead>
		<tbody>
			<?php $no=1 ?>
			@foreach($gajiperbulan as $data)
			<?php $tunjangan = number_format($data->total_tunjangan,0,",",".") ?>
            <?php $dl = number_format($data->total_dl,0,",",".") ?>
            <?php $lembur = number_format($data->total_lembur,0,",",".") ?>
            <?php $gaji = number_format($data->total_gaji,0,",",".") ?>
			<tr>
				<td>{{ $no++ }}</td>
				<td>{{ $data->id_gpb }}</td>
				<td>{{ $data->id }}</td>
				<td><?php echo"Rp$tunjangan" ?> </td>
				<td><?php echo"Rp$dl" ?></td>
				<td><?php echo"Rp$lembur" ?></td>
				<td><?php echo"Rp$gaji" ?></td>
				<td>{{ $data->created_at }}</td>
			</tr>
			@endforeach
		</tbody>
	</table></center>
</body>
</html>