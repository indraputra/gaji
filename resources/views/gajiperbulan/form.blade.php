<div id="create-modal" class="modal fade" role="dialog" aria-hidden="true" tabindex="1" data-backdrop="static">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<form method="post" class="form-horizontal" data-toggle="validator">
			{{csrf_field()}}
			{{method_field('POST')}}

			<div class="modal-header" >
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<center><h4 class="modal-title"></h4></center>
			</div>

			<div class="modal-body">
				@foreach($absen as $data)
				<input type="hidden" name="id" id="id">
				<fieldset disabled>
				<center><h4>{{$data->created_at}}</h1></center>
				<div class="form-group">
					<label for="absen" class="control-label col-sm-2">Hari Masuk</label>
					<div class="col-sm-10">
						<input type="text" class="form-control" id="hari masuk" name="hari masuk" value="{{$data->hari_masuk}} hari" required autofocus>
						<span class="help-block with-errors"></span>
					</div>
				</div>

				<div class="form-group">
					<label for="absen" class="control-label col-sm-2">Total Jam Kerja</label>
					<div class="col-sm-10">
						<input type="text" class="form-control" id="absen" name="absen" value="{{$data->total_jam_kerja}} jam" required autofocus>
						<span class="help-block with-errors"></span>
					</div>
				</div>

				<div class="form-group">
					<label for="absen" class="control-label col-sm-2">Absen</label>
					<div class="col-sm-10">
						<input type="text" class="form-control" id="absen" name="absen" value="{{$data->absen}} hari" required autofocus>
						<span class="help-block with-errors"></span>
					</div>
				</div>

				<div class="form-group">
					<label for="absen" class="control-label col-sm-2">Izin</label>
					<div class="col-sm-10">
						<input type="text" class="form-control" id="absen" name="absen" value="{{$data->izin}} hari" required autofocus>
						<span class="help-block with-errors"></span>
					</div>
				</div>

				<div class="form-group">
					<label for="absen" class="control-label col-sm-2">Terlambat</label>
					<div class="col-sm-10">
						<input type="text" class="form-control" id="absen" name="absen" value="{{$data->terlambat}}x" required autofocus>
						<span class="help-block with-errors"></span>
					</div>
				</div>

				<div class="form-group">
					<label for="absen" class="control-label col-sm-2">Cuti</label>
					<div class="col-sm-10">
						<input type="text" class="form-control" id="absen" name="absen" value="{{$data->cuti}}x" required autofocus>
						<span class="help-block with-errors"></span>
					</div>
				</div>

				<div class="form-group">
					<label for="absen" class="control-label col-sm-2">DL Dalam Kota</label>
					<div class="col-sm-10">
						<input type="text" class="form-control" id="absen" name="absen" value="{{$data->dl_dalam_kota}}x" required autofocus>
						<span class="help-block with-errors"></span>
					</div>
				</div>

				<div class="form-group">
					<label for="absen" class="control-label col-sm-2">DL Luar Kota</label>
					<div class="col-sm-10">
						<input type="text" class="form-control" id="absen" name="absen" value="{{$data->dl_luar_kota}}x" required autofocus>
						<span class="help-block with-errors"></span>
					</div>
				</div>

				<div class="form-group">
					<label for="absen" class="control-label col-sm-2">Lembur Hari Kerja</label>
					<div class="col-sm-10">
						<input type="text" class="form-control" id="absen" name="absen" value="{{$data->lembur_hari_kerja}}x" required autofocus>
						<span class="help-block with-errors"></span>
					</div>
				</div>

				<div class="form-group">
					<label for="absen" class="control-label col-sm-2">Lembur Hari Libur</label>
					<div class="col-sm-10">
						<input type="text" class="form-control" id="absen" name="absen" value="{{$data->lembur_hari_libur}}x" required autofocus>
						<span class="help-block with-errors"></span>
					</div>
				</div>
				<hr>
				</fieldset>

				@endforeach
			</div>

			<div class="modal-footer">
				<button class="btn btn-danger" type="button" data-dismiss="modal">
					<span class="glyphicon glyphicon-remove"></span>Close
				</button>
			</div>

			</form>

		</div>
	</div>
</div>
