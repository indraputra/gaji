<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::get('/home', function () {
    return view('desain.home');
})->name('home');

Auth::routes();

// Route::get('/app', 'AppController@index');

Route::get('/app/karyawan', 'KaryawanController@index')->name('karyawan.index');

Route::get('/app/excelkaryawan', 'KaryawanController@excel')->name('karyawan.excel');

Route::get('/app/pdfkaryawan', 'KaryawanController@pdf')->name('karyawan.pdf');

Route::get('/app/karyawan/create', 'KaryawanController@create')->name('karyawan.create');

Route::post('/app/karyawan/create', 'KaryawanController@store')->name('karyawan.store');

Route::get('/app/karyawan/{karyawan}/edit' , 'KaryawanController@edit')->name('karyawan.edit');

Route::put('/app/karyawan/{karyawan}/edit' , 'KaryawanController@update')->name('karyawan.update');

Route::delete('/app/karyawan/{karyawan}/delete', 'KaryawanController@destroy')->name('karyawan.destroy');

Route::delete('/app/karyawan/del', 'KaryawanController@destroyall')->name('karyawan.destroyall');

Route::get('/app/gaji', 'GajiController@index')->name('gaji.index');

Route::get('/aap/excelgaji', 'GajiController@excel')->name('gaji.excel');

Route::get('/app/pdfgaji', 'GajiController@pdf')->name('gaji.pdf');

Route::get('/app/gaji/create','GajiController@create')->name('gaji.create');

Route::post('/app/gaji/store', 'GajiController@store')->name('gaji.store');

Route::get('/app/gaji/{gaji}/edit' , 'GajiController@edit')->name('gaji.edit');

Route::put('/app/gaji/{gaji}/edit' , 'GajiController@update')->name('gaji.update');

Route::delete('/app/gaji/{gaji}/delete', 'GajiController@destroy')->name('gaji.destroy');

Route::delete('/app/gaji/del', 'GajiController@destroyall')->name('gaji.destroyall');

Route::get('/app/gajikaryawan', 'GajiKaryawanController@index')->name('gajikaryawan.index');

Route::get('/app/pdfgajikaryawan', 'GajiKaryawanController@pdf')->name('gajikaryawan.pdf');

Route::get('/aap/excelgajikaryawan', 'GajiKaryawanController@excel')->name('gajikaryawan.excel');

Route::get('/app/gajikaryawan/create', 'GajiKaryawanController@create')->name('gajikaryawan.create');

Route::post('/app/gajikaryawan/create', 'GajiKaryawanController@store')->name('gajikaryawan.store');

Route::get('/app/gajikaryawan/{gajikaryawan}/edit', 'GajiKaryawanController@edit')->name('gajikaryawan.edit');

Route::put('/app/gajikaryawan/{gajikaryawan}/edit', 'GajiKaryawanController@update')->name('gajikaryawan.update');

Route::delete('App/gajikaryawan/{gajikaryawan}/delete', 'GajiKaryawanController@destroy')
	->name('gajikaryawan.destroy');

Route::delete('App/gajikaryawan/del', 'GajiKaryawanController@destroyall')->name('gajikaryawan.destroyall');

Route::get('/app/gajikaryawan/{gajikaryawan}/addgaji', 'gajiperbulans@addgaji')
	->name('gajiperbulan.addgaji');

Route::get('/app/gajikaryawan/{gajikaryawan}/show', 'GajiPerBulans@show')
	->name('gajiperbulan.show');

Route::get('/aap/excelgajiperbulan', 'GajiPerBulans@excel')->name('gajiperbulan.excel');

Route::get('/app/gajikaryawan/show/{absen}/absen', 'GajiPerBulans@absen')
	->name('gajiperbulan.absen');

Route::get('/app/gajikaryawan/show/{gajiperbulan}/pdfgajiperbulan', 'GajiPerBulans@pdf')
	->name('gajiperbulan.pdf');

Route::delete('/app/gajikaryawan/{id_gpb}/delete', 'GajiPerBulans@destroy')
	->name('gajiperbulan.destroy');

Route::delete('/app/gajikaryawan/del', 'GajiPerBulans@destroyall')
	->name('gajiperbulan.destroyall');

Route::post('/app/gajiperbulan/create', 'GajiPerBulans@store')->name('gajiperbulan.store');

Route::get('/karyawan/search', 'KaryawanController@search')->name('karyawan.search');

Route::get('/gaji/search', 'GajiController@search')->name('gaji.search');

Route::get('/gajikaryawan/search', 'GajiKaryawanController@search')->name('gajikaryawan.search');

Route::get('/gajiperbulan/search/{gajiperbulan}/', 'GajiPerBulans@search')->name('gajiperbulan.search');
